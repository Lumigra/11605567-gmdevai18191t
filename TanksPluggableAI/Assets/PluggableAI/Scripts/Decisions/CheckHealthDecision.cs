﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Complete;

[CreateAssetMenu (menuName = "PluggableAI/Decisions/Check Health")]
public class CheckHealthDecision : Decision
{
    public float HealthThreshold;

    public override bool Decide(StateController controller)
    {
        bool injured = IsWounded(controller);
        return injured;
    }

    private bool IsWounded(StateController controller)
    {
        float currentHealth = controller.GetComponent<TankHealth>().GetCurrentHealth();
        return (currentHealth <= controller.GetComponent<TankHealth>().m_StartingHealth * (HealthThreshold * 0.01));
    }
}
