﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "PluggableAI/Actions/Escape")]
public class RunAction : Action {
    public override void Act(StateController controller)
    {
        Run(controller);
    }

    private void Run(StateController controller)
    {
        controller.navMeshAgent.destination = -controller.chaseTarget.position;
        controller.navMeshAgent.isStopped = false;
        //controller.navMeshAgent.velocity = -controller.enemyStats.moveSpeed;
    }
}
