﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "PluggableAI/State")]
public class State : ScriptableObject
{

    public Action[] actions;
    public Transition[] transitions;
    public Color sceneGizmoColor = Color.gray;

    public void UpdateState(StateController stateController)
    {
        DoActions(stateController);
        CheckTransitions(stateController);
    }

    private void DoActions(StateController Controller)
    {
        for (int i = 0; i < actions.Length; i++)
        {
            actions[i].Act(Controller);
        }
    }

    private void CheckTransitions(StateController controller)
    {
        for (int i = 0; i < transitions.Length; i++)
        {
            bool decisionSucceeded = transitions[i].decision.Decide(controller);

            if(decisionSucceeded)
            {
                controller.TransitiontoState(transitions[i].trueState);
            }
            else
            {
                controller.TransitiontoState(transitions[i].falseState);
            }
        }
    }
}
