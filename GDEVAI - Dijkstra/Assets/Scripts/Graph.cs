﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class Graph : MonoBehaviour {

    [SerializeField]
    protected List<Node> nodes = new List<Node>();

    public virtual List<Node> GetNodes
    {
        get { return nodes; }
    }

    public virtual Path GetShortestPath(Node Start, Node End)
    {
        //if no start or end point, don't run
        if (Start == null || End == null)
        {
            throw new ArgumentNullException();
        }
        Path path = new Path();

        // if at the point already, stop running
        if(Start == End)
        {
            path.GetNodes.Add(Start);
            return path;
        }

        List<Node> unvisited = new List<Node>();

        //previous nodes in optimal path from source
        //Dictionary is a list
        /*Dictionary<Node, float>
         * Node A, 0
         * Node B, 4
         */
        Dictionary<Node, Node> previous = new Dictionary<Node, Node>();

        //calculated distance
        Dictionary<Node, float> distances = new Dictionary<Node, float>();

        //set all nodes to infinity except start node
        for (int i= 0; i< nodes.Count;i++)
        {
            Node node = nodes[i];
            unvisited.Add(node);

            //set distance to infinity
            distances.Add(node, float.MaxValue);
        }

        //set start to 0 distance
        distances[Start] = 0f;

        while(unvisited.Count != 0)
        {
            // Sort unvisited list by distance, smallest at start, largest at end
            unvisited = unvisited.OrderBy(node => distances[node]).ToList();
            //                                 ^lambda


            //Get node with smallest distance
            Node current = unvisited[0];

            unvisited.Remove(current);

            //When current node is equal to end, break

            if(current == End)
            {
                while (previous.ContainsKey(current))
                {
                    //insert node to final list
                    path.GetNodes.Insert(0, current);

                    //tranverse from start to end
                    current = previous[current];
                }

                //Insert source onto final list
                path.GetNodes.Insert(0, current);
                break;
            }

            //looping through the Node connections (neighbors) and where the connection is available at unvisited list
            for(int i = 0; i  < current.GetConnections.Count;i++)
            {
                Node neighbor = current.GetConnections[i];

                //Getting the distance between the current node and the connection
                float length = Vector3.Distance(current.transform.position, neighbor.transform.position);

                //Distance from start node to this connection
                float alt = distances[current] + length;

                //Shorter path has been found. if still infinity or higher, make the value lower
                if(alt<distances[neighbor])
                {
                    distances[neighbor] = alt;
                    previous[neighbor] = current;
                }
            }
        }

        path.Bake();
        return path;
    }
}
