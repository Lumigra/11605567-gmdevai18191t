﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {
    public int MaxHealth;
    private int currentHealth;
	// Use this for initialization
	void Start () {
        currentHealth = MaxHealth;
	}
	
    public void takeDamage(int damage)
    {
        currentHealth -= damage;
        if (currentHealth <= 0) Die();
    }

    public void Heal(int healValue)
    {
        currentHealth += healValue;
        if (currentHealth > MaxHealth) currentHealth = MaxHealth;
    }

    private void Die()
    {
        Destroy(gameObject);
    }
}
