﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntiAir : Tower_BaseClass {
    public Bullet bullet;
	// Use this for initialization
	void Start () {
        isShooting = false;
	}
    protected override void Update()
    {
        base.Update();
        if (!isShooting)
        {
            StartCoroutine(DoAction());
            isShooting = true;
        }
        if (currentTarget == null) isShooting = false;
    }
    // Update is called once per frame
    new void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Follower>().GetTerrain() == Follower.Terrain.AIR) targets.Add(collision.gameObject);
    }

    protected override IEnumerator DoAction()
    {
        if (currentTarget != null)
        {
            Bullet toSpawn = bullet;
            toSpawn.target = currentTarget;
            Instantiate(bullet, transform.position, Quaternion.identity);

            yield return new WaitForSeconds(FireRate);

            StartCoroutine(DoAction());
        }
    }
}
