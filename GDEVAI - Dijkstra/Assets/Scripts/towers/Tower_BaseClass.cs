﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower_BaseClass : MonoBehaviour {
    protected bool isShooting;
    public float FireRate;
    public GameObject currentTarget;
    public List<GameObject> targets = new List<GameObject>();
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	protected virtual void Update () {
        if (targets.Count != 0) currentTarget = targets[0];
        else currentTarget = null;
	}

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        targets.Add(collision.gameObject);
    }

    protected virtual IEnumerator DoAction()
    {
        yield return null;
    }

    protected void OnTriggerExit2D(Collider2D collision)
    {
        targets.Remove(collision.gameObject);
    }
}
