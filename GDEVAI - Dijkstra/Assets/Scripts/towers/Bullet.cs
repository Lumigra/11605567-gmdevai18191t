﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
    public float speed;
    public int damage;
    public GameObject target;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = Vector2.MoveTowards(transform.position, target.transform.position, speed);
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        collision.collider.gameObject.GetComponent<Health>().takeDamage(damage);
        Destroy(gameObject);
    }
}
