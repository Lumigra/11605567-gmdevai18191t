﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Node : MonoBehaviour {


    //neighboring nodes, the ones you add in the editor
    [SerializeField]
    protected List<Node> connections = new List<Node>();

    
    public virtual List<Node> GetConnections
    {
        get
        {
            return connections;
        }
    }

    public Node this [int index]
    {
        get
        {
            return connections[index];
        }
    }

    private void onValidate()
    {
        connections = connections.Distinct().ToList();
    }
}
