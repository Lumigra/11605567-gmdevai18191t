﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node {
    //possition in node array
    public int GridPosX;
    public int GridposY;


    //tells if node is obstructed
    public bool isWall;
    // Use this for initialization

    public Vector3 Position;

    //stores previous node
    public Node ParentNode;

    //cost of previous nodes
    public int G_Cost;
    /* distance to goal from this node
     * heuristic
     * */
    public int H_Cost;

    /*
     * Quic getter to add G_Cost and cost
     * 
     * A* formula = 
     * 
     * F(N) = G(N) + H(N)
     * */
    public int F_Cost
    {
        get
        {
            return G_Cost + H_Cost;
        }
    }

    public Node(bool isWall, Vector3 worldPos, int gridX, int gridY)
    {
        this.isWall = isWall;
        this.Position = worldPos;
        this.GridPosX = gridX;
        this.GridposY = gridY;
    }
}
