﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class Follower : MonoBehaviour {
    public Grid grid;

    public Transform originalPos;
    public float MaxSpeed;
    public float currentSpeed;

    private List<Node> path;
    private Node current;
    // Use this for initialization

    private void Start()
    {
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) transform.position = originalPos.position;

        if (path != null && current != path[path.Count()-1])
        {
            if (grid.IsTraversable) currentSpeed = MaxSpeed;
            else currentSpeed = 0;
            if (current == null)
            {
                current = path[0];
                transform.position = current.Position;
            }
            else if (path.Count() > 1)
            {
                current = path[1];
                transform.position = Vector3.MoveTowards(transform.position, current.Position, currentSpeed);
            }
        }
    }

    private void LateUpdate()
    {
        path = grid.FinalPath;
    }
}
