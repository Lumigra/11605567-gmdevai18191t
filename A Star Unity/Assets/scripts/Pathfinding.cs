﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class Pathfinding : MonoBehaviour {
    public Grid grid;
    public Transform startPos, targetPos;

    private void Awake()
    {
        grid = GetComponent<Grid>();
    }
    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void LateUpdate () {
        FindPath(startPos.position, targetPos.position);
        if (grid.FinalPath != null)
        {
            Node last;
            last = grid.FinalPath[grid.FinalPath.Count - 1];
            if (last == grid.NodeFromWoldPoint(targetPos.position)) grid.IsTraversable = true;
            else grid.IsTraversable = false;
        }
    }

    void FindPath (Vector3 start, Vector3 target)
    {
        Node startNode = grid.NodeFromWoldPoint(start);
        Node targetNode = grid.NodeFromWoldPoint(target);

        List<Node> openNodes = new List<Node>();
        HashSet<Node> closedNodes = new HashSet<Node>();

        openNodes.Add(startNode); //add start node to begin algorithm

        while(openNodes.Count > 0)
        {
            Node currentNode = openNodes[0]; // create a node and set it to first node
            for (int i = 1; i < openNodes.Count; i++)
            {
                // if F_Cost if less than or equal to current node
                if (openNodes[i].F_Cost < currentNode.F_Cost ||
                    openNodes[i].F_Cost == currentNode.F_Cost &&
                    openNodes[i].F_Cost < currentNode.H_Cost)
                {
                    currentNode = openNodes[i]; // set current node to that object
                }
            }

            openNodes.Remove(currentNode);
            closedNodes.Add(currentNode);

            if (currentNode == targetNode)
            {
                getFinalPath(startNode, targetNode);
            }


            // obstacle detection
            foreach(Node neighbor in grid.GetNeighboringNode(currentNode)) // loop through each neighbor of the current node
            {
                // if neighbor is not a wall or has been already checked
                if(!neighbor.isWall || closedNodes.Contains(neighbor))
                {
                    continue; // skip node
                }

                int moveCost = currentNode.G_Cost + getManhattanDistance(currentNode, neighbor);

                if(moveCost < neighbor.G_Cost || !openNodes.Contains(neighbor)) // if f_cost is greater than g_cost or is not in the open list
                {
                    neighbor.G_Cost = moveCost;
                    neighbor.H_Cost = getManhattanDistance(neighbor, targetNode);
                    neighbor.ParentNode = currentNode;

                    if (!openNodes.Contains(neighbor))
                    {
                        openNodes.Add(neighbor);
                    }
                }
            }
            
        }

    }

    void getFinalPath (Node start, Node end)
    {
        List<Node> finalPath = new List<Node>();
        Node currentNode = end;

        while(currentNode != start)
        {
            finalPath.Add(currentNode);
            currentNode = currentNode.ParentNode;
        }
        finalPath.Reverse();
        grid.FinalPath = finalPath;
    }

    int getManhattanDistance(Node nodeA, Node nodeB)
    {
        int x = Mathf.Abs(nodeA.GridPosX - nodeB.GridPosX);
        int y = Mathf.Abs(nodeA.GridposY - nodeB.GridposY);

        return x + y;
    }
}
